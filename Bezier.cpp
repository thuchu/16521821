#include "Bezier.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	double t;
	for(i=0.0;i<1.0;t+=0.0005)
	{
		double xt=pow(1-t,2)*p1.x+2*(1-t)*t*p2.x+t*t*p3.x;
		double yt=pow(1-t,2)*p1.x+2*(1-t)*t*p2.y+t*t*p3.y;
		SDL_RenderDrawPoint(ren, xt, yt);
	}
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	double t;
	for(i=0.0;i<1.0;t+=0.0005)
	{
		double xt=pow(1-t,3)*p1.x+3*pow(1-t,3)*t*t*p2.x+3*(1-t)*t*t*p3.x+t*t*t*p4.x;
		double yt=pow(1-t,3)*p1.y+3*pow(1-t,3)*t*t*p2.y+3*(1-t)*t*t*p3.y+t*t*t*p4.y;
		SDL_RenderDrawPoint(ren, xt, yt);
	}
}