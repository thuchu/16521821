#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
    RECT rect;
    rect.Left = l;
    rect.Right = r;
    rect.Top = t;
    rect.Bottom = b;

    return rect;
}

CODE Encode(RECT r, Vector2D P)
{
    CODE c = 0;
    if (P.x < r.Left)
        c = c|LEFT;
    if (P.x > r.Right)
        c = c|RIGHT;
    if (P.y < r.Top)
        c = c|TOP;
    if (P.y > r.Bottom)
        c = c|BOTTOM;
    return c;
}

void SwapPoint(Vector2D &P1, Vector2D &P2, CODE &c1, CODE &c2)
{
	if(!c1) // Neu p1 nam hoan toan trong cua so
	{
		Vector2D p;
		p = P1;
		P1 = P2;
		P2 = p;
		
		CODE c;
		c = c1;
		c1 = c2;
		c2 = c;
	}
}

int CheckCase(int c1, int c2)
{
    if (c1 == 0 && c2 == 0)
        return 1;
   if ((c1 != 0) && (c2 != 0) && ((c1&c2) != 0))
        return 2;
    return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	CODE c1,c2;
	int Stop=0, Result=0;
	while(Stop==0)
	{
		c1=Encode(r,P1);
		c2=Encode(r,P2);
		if(Check(c1,c2)==1)
		{
			Stop==1;
			Result=1;
		}else
		{
			if(Check(c1,c2)==2)
			{
				Stop==1;
			}else
			{
				SwapPoint(P1,P2,c1,c2);
				float a;
				if(P2.x!=P1.x)
				m=float(P2.y-P1.y)/(P2.x-P1.x);
				if(c1 & LEFT)
				{
					P1.y+=(r.Left-P1.x)*m;
					P1.x=r.Left;
				}else
				{
					if(c1 & RIGHT)
					{
						P1.y+=(r.Right-P1.x)*m;
						P1.x=r.Right;
					}else
					{
						if(c1 & TOP)
						{
							if(P2.x!=P1.x)
							P1.x+=(r.Top-P1.y)/m;
							P1.y=r.Top;
						}else
						{
							if(P2.x!=P1.x)
							P1.x+=(r.Bottom-P1.y)/m;
							P1.y=r.bottom;
						}
					}
				}
			}
		}
	}
	Q1=P1;
	Q2=P2;
	return 1;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	Vector2D Q1,Q2;
	CohenSutherland(r,P1,P2,Q1,Q2);
	P1=Q1;
	P2=Q2;
}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
    if (p == 0)
    {
        if (q < 0)
            return 0;
        return 1;
    }

    if (p > 0)
    {
        float t=(float)q/p;
        if(t2<t)
            return 1;
        if(t<t1)
            return 0;
        t2 = t;
        return 1;
    }

    float t=(float)q/p;
    if(t2<t)
        return 0;
    if(t<t1)
        return 1;
    t1 = t;
    return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	
}
