#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc+x, yc-y);
	SDL_RenderDrawPoint(ren, xc-x, yc-y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	if(A==0)
	{
		return;
	}
	float x=0,y=0,d=0,moc=1;
	if(A<0)
	{
		A=-A;
	}
	while(x*A<0.5)
	{
		Draw2Points(xc,yc,x,y*moc,ren);
		d+=A*(2*x+1);
		if(y+0.5<d)
		y++;
		x++;
	}
	while(y<yc)
	{
		Draw2Points(xc,yc,x,y*moc,ren);
		y++;
		if(d+A*(x+0.5)<y)
		{
			d+=A*(2*x+1);
			x++;
		}
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
}
